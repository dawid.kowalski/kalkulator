from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QLabel, QGridLayout, QLineEdit, QPushButton, QHBoxLayout, QMessageBox
class Kalkulator(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.interfejs()

    def interfejs(self):
        self.l1_l = QLabel("Liczba 1: ", self)
        self.l2_l = QLabel("Loczba 2: ", self)
        self.wynik_l = QLabel("Wynik: ", self)
        self.l1 = QLineEdit()
        self.l2 = QLineEdit()
        self.wynik = QLineEdit()
        self.wynik.readonly = True
        dodaj = QPushButton("&Dodaj", self)
        usun = QPushButton("&Odejmowanie", self)
        mnozenie = QPushButton("&Mnożenie", self)
        dziel = QPushButton("&Podziel", self)
        uklad = QGridLayout()
        uklad.addWidget(self.l1_l, 0, 0)
        uklad.addWidget(self.l2_l, 0, 1)
        uklad.addWidget(self.wynik_l, 0, 2)
        uklad.addWidget(self.l1, 1, 0)
        uklad.addWidget(self.l2, 1, 1)
        uklad.addWidget(self.wynik, 1, 2)
        ukladH = QHBoxLayout()
        ukladH.addWidget(dodaj)
        ukladH.addWidget(usun)
        ukladH.addWidget(mnozenie)
        ukladH.addWidget(dziel)
        uklad.addLayout(ukladH, 2, 0, 1,3)
        self.setLayout(uklad)
        dodaj.clicked.connect(self.dodawanie)
        usun.clicked.connect(self.odejmowanie)
        mnozenie.clicked.connect(self.mnozenie)
        dziel.clicked.connect(self.dzielenie)
        self.resize(300, 100)
        self.setWindowTitle("Prosty kalkulator")
        self.show()
    def dodawanie(self):
        liczba1 = self.l1.text()
        liczba2 = self.l2.text()
        if liczba1 != "" and liczba2 != "":
            liczba1 = float(liczba1)
            liczba2 = float(liczba2)
            wynik = liczba1 + liczba2
            self.wynik.setText(str(wynik))
        else:
            QMessageBox.information(self, "Brak danych", "Obje liczby muszą być podane")

    def odejmowanie(self):
        liczba1 = self.l1.text()
        liczba2 = self.l2.text()
        if liczba1 != "" and liczba2 != "":
            liczba1 = float(liczba1)
            liczba2 = float(liczba2)
            wynik = liczba1 - liczba2
            self.wynik.setText(str(wynik))
        else:
            QMessageBox.information(self, "Brak danych", "Obje liczby muszą być podane")
    def mnozenie(self):
        liczba1 = self.l1.text()
        liczba2 = self.l2.text()
        if liczba1 != "" and liczba2 != "":
            liczba1 = float(liczba1)
            liczba2 = float(liczba2)
            wynik = liczba1 * liczba2
            self.wynik.setText(str(wynik))
        else:
            QMessageBox.information(self, "Brak danych", "Obje liczby muszą być podane")

    def dzielenie(self):
        liczba1 = self.l1.text()
        liczba2 = self.l2.text()
        if liczba1 != "" and liczba2 != "":
            if liczba2 != 0:
                liczba1 = float(liczba1)
                liczba2 = float(liczba2)
                wynik = liczba1 + liczba2
                self.wynik.setText(str(wynik))
            else:
                QMessageBox.critical(self, "Błąd", "Nie można dzielić przez 0")
        else:
            QMessageBox.information(self, "Brak danych", "Obje liczby muszą być podane")

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    okno = Kalkulator()
    sys.exit(app.exec_())
